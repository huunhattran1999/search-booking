import React, { Component } from 'react';
import './style.css'
import axios from 'axios';
import Modal from 'react-bootstrap/Modal'
import Moment from 'react-moment';

class CTV extends Component {
    constructor(props) {
        super(props);
        this.state = {
            phone:"",
            itemData:[],
            itemBooking:[],
            modal:false,
            item:{},
        }
    }
    handleTrue=()=>{this.setState({modal:true})}
    handleFalse=()=>{this.setState({modal:false})}
    handleChange=(event)=>{
        this.setState({phone :event.target.value})
    }
    detail=(item)=>{
        this.setState({modal:true, item})
    } 
    handleDetail=(item)=>{
        this.setState({item})
        console.log(item);
        let headers = {
            'Authorization':'JWT eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7Il9pZCI6IjVjYzdlYmJhM2RiZjBmMjMyOWM1ZDM0NiIsInJvbGUiOiJtb2RlcmF0b3IiLCJjbGluaWNJZCI6IjVjMzgwOTVkYzdlMWZhNDkzMzUwZGMyZCIsInZlcnNpb25Ub2tlbiI6IjEuMCIsInZlcnNpb25TdGFmZiI6IjEuMCJ9LCJleHAiOjE2MzgzNDc3NjYsImlhdCI6MTYyMjUzNjU2Nn0.dI9kpD1qYa56X8etXLJaZT6o62sT3OwwiYtpOMeR5jY',
        };
        axios.get(`https://api.weva.vn/bookings/electronic-records?customerId=${item._id}&sortBy=time`,{headers})
        .then(
            res => {
                let data = res.data.bookings
                this.setState({itemBooking:data,item})
                return this.handleTrue()
            }
        )
    }
    handleSearch=()=>{
        var phone = this.state.phone
            if(phone[0]==="0"){
                phone = phone.slice(0,phone.length)
            }
        let headers = {
            'Authorization':'JWT eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7Il9pZCI6IjVjYzdlYmJhM2RiZjBmMjMyOWM1ZDM0NiIsInJvbGUiOiJtb2RlcmF0b3IiLCJjbGluaWNJZCI6IjVjMzgwOTVkYzdlMWZhNDkzMzUwZGMyZCIsInZlcnNpb25Ub2tlbiI6IjEuMCIsInZlcnNpb25TdGFmZiI6IjEuMCJ9LCJleHAiOjE2MzgzNDc3NjYsImlhdCI6MTYyMjUzNjU2Nn0.dI9kpD1qYa56X8etXLJaZT6o62sT3OwwiYtpOMeR5jY',
        };
        console.log(`https://api.weva.vn/customers?search=${phone}&limit=15&skip=0`);
        axios.get(`https://api.weva.vn/customers?search=${phone}&limit=15&skip=0`,{headers}) 
        .then(
            res => {
                let data = res.data.customers
                console.log("kkkkkk",data);
                this.setState({itemData:data})
            }
        )
    }
    componentDidMount=(item)=>{
        this.setState(item)
        var phone = this.state.phone
        let headers = {
            'Authorization':'JWT eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7Il9pZCI6IjVjYzdlYmJhM2RiZjBmMjMyOWM1ZDM0NiIsInJvbGUiOiJtb2RlcmF0b3IiLCJjbGluaWNJZCI6IjVjMzgwOTVkYzdlMWZhNDkzMzUwZGMyZCIsInZlcnNpb25Ub2tlbiI6IjEuMCIsInZlcnNpb25TdGFmZiI6IjEuMCJ9LCJleHAiOjE2MzgzNDc3NjYsImlhdCI6MTYyMjUzNjU2Nn0.dI9kpD1qYa56X8etXLJaZT6o62sT3OwwiYtpOMeR5jY',
        };
        axios.get(`https://api.weva.vn/customers?search=${phone}&limit=5&skip=0`,{headers}) 
        .then(
            res => {
                let data = res.data.customers
                this.setState({itemData:data})
            }
        )
    }
    render() {
        this.state.itemData.forEach((item)=>{
            if(item.group==='used'){
                return item.group='Đã sữ dụng '
            }
            if(item.group==='booked'){
                return item.group='Đã đặt hẹn'
            }
        })
        // var{phone}=this.state
        // var filterPhone = this.state.itemData.filter(item=>{
        //     return item.phone.toLowerCase().indexOf(phone.toLocaleLowerCase())!==-1
        // })
        return (
            <div id="page">
                <div className="header">
                    <div className="container">
                        <div className="row">
                            <div className="col-md-12">
                                <div className="logo">
                                    <img src="logo.png" alt="" width="100%"></img>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="main">
                    <div className="container">
                        <div className="row">
                            <div className="col-md-12">
                                <div className="item-customer">
                                    <div className="row">
                                        <div className="col-md-12">
                                            <label>Khách hàng</label>
                                        </div>
                                        <div className="col-md-12">
                                            <input placeholder="Nhập số điện thoại..." type="text" className="item-search" name="phone"  onChange={(e)=>this.handleChange(e)}></input>
                                            <i className="fi-rr-search" onClick={()=>this.handleSearch()}></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-12">
                                <div className="item-list">
                                    <div className="container">
                                    {this.state.itemData.map((item,i )=> (
                                    <div className="row" key={i} >
                                        <div className="space">
                                            <div className="row">
                                                <div className="col-md-12">
                                                    <div className="dashed"></div>
                                                    <label className="time"><Moment format="DD/MM/YYYY">{item.updatedAt}</Moment></label>
                                                    <div className="dash"></div>
                                                </div>
                                                <div className="col-md-4"></div>
                                                <div className="col-md-4"> </div>
                                            </div>
                                           
                                        </div>
                                        <div className="item-list-column"  onClick={()=>this.handleDetail(item)}>
                                            <div className="container">                                            
                                                <div className="item-column-img">
                                                    <div className="row">
                                                        <div className="col-md-12">
                                                            <div className="item-img">
                                                                <img src="https://img.icons8.com/fluent/60/000000/user-male-circle.png" width="100%" alt=""/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="item-column-one">
                                                    <div className="row">
                                                        <div className="col-md-12">
                                                            <div className="item-list-first">
                                                                <label className="item-name">{item.fullname}</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="item-column-two">
                                                    <div className="row">
                                                        <div className="col-md-12">
                                                            <div className="item-list-second">
                                                                <label className="item-group"><label>{item.group}</label></label>
                                                                <label className="item-time"><Moment format="hh:mm DD/MM/YYYY">{item.updatedAt}</Moment></label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="item-column-third">
                                                    <div className="row">
                                                        <div className="col-md-12">
                                                            <div className="item-list-thirst">
                                                            <label className="item-service">Sale : <label>{item.sale.fullname}</label> </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>  
                                    </div>
                                    ))} 
                                    </div>
                                </div>
                            </div>
                        </div>
                            <Modal show={this.state.modal} onHide={this.handleFalse}>
                                <Modal.Header closeButton>
                                <Modal.Title>{this.state.item.fullname}</Modal.Title>
                                <div className="phone"><p>(+84) {this.state.item.phone}</p></div>
                                </Modal.Header>
                                <Modal.Body>
                                    {/* <div className="container"> */}
                                    <div className="item-history">
                                            <div className="row" >
                                            { this.state.itemBooking.length?
                                            <div className="list">
                                                {this.state.itemBooking.map((item,i)=>(
                                                    <div>
                                                <div className="col-md-12" key={i}>
                                                    <label className="booking">Booking : {i+1}</label>
                                                    <div className="list-his">
                                                        <label className="item-name"><label className="weight">Check-in</label> : <Moment format="hh:mm - DD/MM/YYYY">{item.time}</Moment></label>
                                                    </div>
                                                </div>
                                                <div className="col-md-12">
                                                    <div className="list-history">
                                                        <label className="item-name"><label className="weight">Dịch vụ</label> : <label>{item.services}</label></label>
                                                    </div>
                                                </div>
                                                <div className="col-md-12">
                                                    <div className="list-history">
                                                        <label className="item-name"><label className="weight">Tổng tiền</label> : <label>{Number(item.totalMoney).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")+" VND"}</label></label>
                                                    </div>
                                                </div>
                                                <div className="col-md-12">
                                                    <div className="list-history">
                                                        <label className="item-name"><label className="weight">Checkout</label> : <label>{item.checkOutField.branch}{" - "}<Moment format="hh:mm - DD/MM/YYYY">{item.checkOutField.time}</Moment></label></label>
                                                    </div>
                                                </div>
                                                <div className="col-md-12">
                                                    <div className="list-note">
                                                        <label className="item-name"><label className="weight">Ghi chú</label> : <label>{item.note}</label></label>
                                                    </div>
                                                </div>
                                                </div>
                                               
                                                ))}

                                                 </div>
                                                : <div></div>}
                                            </div>
                                    </div>
                                </Modal.Body>
                            </Modal> 
                    </div>
                </div>
            </div>
        );
    }
}

export default CTV;